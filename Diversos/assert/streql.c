#include <assert.h>

int streql (const char *a, const char *b)
{
  int eql = 1;
  for (int i = 0; eql; ++i) {
    eql &= (a[i] == b[i]);
    if ((a[i] == 0) || (b[i] == 0)) { break; }
  }
  return eql;
}

int main ()
{
  assert(streql("", "") == 1);
  assert(streql("a", "a") == 1);
  assert(streql("a", "b") == 0);
  assert(streql("rafael", "rafael") == 1);
  assert(streql("RAFAEL", "RAFAEL") == 1);
  assert(streql("RAFAEL", "rafael") == 0);
  assert(streql("rafael", "rafa") == 0);
  assert(streql("xXxXx", "XxXxX") == 1);

  return 0;
}

