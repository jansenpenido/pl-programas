/** @file common.c
 *
 * Funções comuns aos algoritmos de ordenação e programas de demonstração.
 *
 */


#include <stdlib.h>
#include <stdio.h>

#include "common.h"


/**
 *
 * Efetua a permutação dos valores de duas variáveis.
 *
 * @param a
 * @param b os endereços dos dois valores a serem permutados.
 *
 */
void swap (int *a, int *b)
{
  int tmp = *a;
  *a = *b;
  *b = tmp;
}


/**
 *
 * Embaralha os elementos de um arranjo.
 *
 * @param arr O arranjo que será embaralhado.
 * @param sz  O número total de elementos contidos no arranjo.
 *
 */
void shuffle (int *arr, unsigned int sz)
{
  for (int i = 0; i < sz; ++i) {
    unsigned int ix = ((unsigned int)rand()) % sz;
    swap(&arr[i], &arr[ix]);
  }
}


/**
 *
 * Gera um arranjo de um dado comprimento com números aleatórios.
 *
 * @param sz  O total de elementos a serem gerados pelo arranjo.
 *
 */
int *generate_array (unsigned int sz)
{
  int *arr = 0;

  if (sz > 0) {
    arr = malloc(sz * sizeof(int));
  }

  if (arr != 0) {
    for (int i = 0; i < sz; ++i) {
      arr[i] = 50 - rand() % 100;
    }
  }

  return arr;
}


/**
 *
 * Imprime e formata um arranjo no terminal.
 *
 * @param arr O arranjo a ser impresso.
 * @param sz  O total de elementos contidos no arranjo.
 *
 */
void show_array (int *arr, unsigned int sz)
{
  for (int i = 0; i < sz; ++i) {
    printf("%3d", arr[i]);
    if (i + 1 < sz) { printf(", "); }
  }
  printf("\n\n");
}


/**
 *
 * Exibe o comportamento de um algoritmo de ordenação sobre um arranjo de
 * números inteiros aleatórios.
 *
 * @param sz        O número de elementos aleatórios a serem gerados para a
 *                  ordenação.
 * @param algorithm Callback. A função de ordenação à ser usada.
 * @param algname   O nome do algoritmo, que será impresso no terminal.
 *
 */
void show_algorithm (
  unsigned int sz, void algorithm(int *, unsigned int), char *algname
)
{
  int *arr = generate_array(sz);

  if (algorithm && arr) {
    printf("\nDemonstração do algortimo \"%s\".\n\n", algname ? algname : "");
    printf("O arranjo antes da ordenação:\n");
    show_array(arr, sz);

    algorithm(arr, sz);

    printf("O arranjo após a ordenação \"%s\":\n", algname ? algname : "");
    show_array(arr, sz);
  }
}

