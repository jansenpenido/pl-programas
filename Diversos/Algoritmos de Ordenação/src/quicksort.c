/** @file quicksort.c
 *
 * Programa de demonstração do algoritmo "Quicksort".
 *
 */


#include "common.h"


/**
 * FIXME: Reorganiza localmente um sub-arranjo e retorna o índice do elemento
 *        pivô.
 *
 * @param arr O arranjo a ser ordenado.
 * @param esq O índice esquerdo do sub-arranjo.
 * @param dir O índice direito do sub-arranjo.
 *
 * @return    O índice do pivô.
 *
 */
int partition (int *arr, int esq, int dir)
{
  // Selecionamos como pivô um valor do meio do arranjo.
  int pivo = arr[esq + (dir - esq) / 2];

  // Preparamos dois cursores, atribuindo a estes os valores dos índices das
  // extremidades do arranjo.
  int i = esq;
  int j = dir;

  while (i <= j) {
    // Movendo os cursores, partindo das extremidades até a posição final do
    // pivô, confrontamos os valores do arranjo indicados nos cursores com o
    // valor do pivô, até coincidirmos os cursores e obter uma partição.
    while (arr[i] < pivo) { ++i; }
    while (arr[j] > pivo) { --j; }

    if (i <= j) {
      // Enquanto os índices não coincidem, efetuamos as permutações
      // necessárias para a ordenação.
      swap(&arr[i], &arr[j]);

      // Após cada permutação, deslocamos o cursor esquerdo, indicando de onde
      // as comparações devem continuar.
      --j;
    }
  }

  // Ao sair do laço, retornamos o valor do cursor direito, que está
  // posicionado sobre o pivô.
  return i;
}


/**
 *
 * Implementação do algoritmo de ordenação Quicksort.
 *
 * @param arr O (sub-)arranjo a ser ordenado.
 * @param esq O índice esquerdo do sub-arranjo.
 * @param dir O índice direito do sub-arranjo.
 *
 */
void do_quicksort (int *arr, int esq, int dir)
{
  if (esq < dir) {
    // Na primeira iteração, os elementos são reorganizados, do primeiro ao
    // último, e então é determinado o elemento pivô.
    int pivo = partition(arr, esq, dir);

    // Em torno desse pivô são definidos dois sub-arranjos, tal que
    //
    //    arranjo[esq..pivo - 1] <= arranjo[pivo] <= arranjo[pivo + 1, dir].
    //
    // Esses sub-arranjos são reordenados recursivamente.
    do_quicksort(arr, esq, pivo - 1);
    do_quicksort(arr, pivo + 1, dir);
  }
}


/**
 *
 * Ponto de entrada da função de ordenação.
 *
 * @param arr O arranjo a ser ordenado.
 * @param sz  O comprimento total do arranjo.
 *
 */
void quicksort (int *arr, unsigned int sz)
{
  // A ordenação é feita na função 'do_quicksort()'.
  do_quicksort(arr, 0, sz - 1);
}


int main ()
{
  show_algorithm(20U, quicksort, "Quicksort");
  return 0;
}

