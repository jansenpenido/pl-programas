/** @file common.c
 *
 * Funções comuns aos algoritmos de ordenação e programas de demonstração.
 *
 */

#ifndef __ALGORITHMS_COMMON_H__
#define __ALGORITHMS_COMMON_H__


/// Efetua a permutação dos valoes de duas variáveis.
void swap (int *, int *);

/// Embaralha os elementos de um arranjo.
void shuffle (int *, unsigned int);


/// Exibe o comportamento de um algoritmo de ordenação sobre um arranjo de
/// números inteiros aleatórios.
void show_algorithm (unsigned int, void(int *, unsigned int), char *);


#endif // __ALGORITHMS_COMMON_H__

