/** @file combsort.c
 *
 * Programa de demonstração do algoritmo "Comb sort".
 *
 */


#include "common.h"


/**
 *
 * Aplica o algoritmo "Comb sort" sobre um arranjo de números inteiros.
 *
 * Este algoritmo é uma variação do "Bubble sort".
 *
 * A novidade aqui é o ajuste de uma lacuna e de um "fator de redução" dessa
 * lacuna, a cada iteração do loop, na intenção de ordenar os valores baixos
 * próximos do fim da sequência, que retardam o desempenho da ordenação de um
 * "Bubble sort" normal.
 *
 * O valor do fator de redução, usado no programa, é definido pela expressão
 * '1 / (1 - e ^ -φ)' onde 'e' é a constante Napier, e a constante 'φ' é a
 * chamada 'proporção áurea'. Esse número poderia ser substituído por algo como
 * '1.3'
 *
 * @param arr O arranjo a ser ordenado.
 * @param sz  O comprimento total do arranjo.
 *
 */
void combsort (int *arr, unsigned int sz) {
  const float shrink_factor = 1.247330950103979f; // Fator de redução
  unsigned int gap = sz;                          // Tamanho da lacuna
  int swapped;                                    // Indicador de permutação

  do {
    swapped = 0;

    // No início do algoritmo, a lacuna definida para o tamanho do arranjo.
    // Antes da primeira iteração, e a cada etapa do laço, a lacuna é reduzida
    // dividindo seu valor pelo "fator de redução". Conforme este valor se
    // aproxima de zero, o comportamento do algoritmo fica mais parecido com o
    // do "Bubble sort".
    if (gap > 1) { gap /= shrink_factor; }

    for (unsigned int i = 0; (gap + i) < sz; ++i) {
      // As comparações e eventuais permutações ocorrem entre os valores
      // separados pela lacuna.
      if (arr[i] > arr[i + gap]) {
        swap(&arr[i], &arr[i + gap]);
        swapped = 1;
      }
    }

  // A parada do laço é determinada quando a lacuna se fecha e as permutações
  // deixam de ocorrer.
  } while ((gap > 1) || swapped);
}


int main ()
{
  show_algorithm(20U, combsort, "Comb sort");
  return 0;
}

