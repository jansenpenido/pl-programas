/** @file bogosort.c
 *
 * Programa de demonstração do algoritmo "Bogosort".
 *
 */


#include "common.h"


/**
 *
 * Aplica o algoritmo "Bogosort" sobre um arranjo de números inteiros.
 *
 * A proposta do algoritmo é simplesmente demonstrar um método ineficiente em
 * relação a algoritmos mais realistas de ordenação. O objetivo é embaralhar um
 * arranjo, até que este, por coincidência, esteja ordenado.
 *
 * @param arr O arranjo a ser ordenado.
 * @param sz  O comprimento total do arranjo.
 *
 */
void bogosort (int *arr, unsigned int sz)
{
  // Usando uma ideia similar a do algoritmo "Bubble sort", declaramos um
  // indicador que será a condição de parada do laço.
  int shuffled;

  do {
    // A cada volta do laço, o indicador é sinalizado como 'falso'.
    shuffled = 0;

    // Iteramos o arranjo inteiro, partindo do segundo elemento em diante,
    // efetuando as comparações entre seus elementos.
    for (int i = 1; i < sz; ++i) {
      // Se o elemento anterior é maior que o elemento comparado, efetuamos o
      // embaralhamento dos elementos do arranjo, e sinalizamos este
      // embaralhamento.
      if (arr[i - 1] > arr[i]) {
        shuffle(arr, sz);
        shuffled = 1;
      }
    }

  // O arranjo estará ordenado não ocorrerem mais embaralhamentos. O laço então
  // será concluído.
  } while (shuffled);
}


int main ()
{
  // Quanto mais elementos, pior a performance do algoritmo.
  // Neste caso, fixei o valor para 12 elementos.
  show_algorithm(12U, bogosort, "Bogosort");
  return 0;
}

