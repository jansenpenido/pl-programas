/** @file bubblesort.c
 *
 * Programa de demonstração do algoritmo "Bubble sort".
 *
 */


#include "common.h"

/**
 *
 * Aplica o algoritmo "Bubble sort" sobre um arranjo de números inteiros.
 * Esta é a versão mais comum, não otimizada do algoritmo.
 *
 * @param arr O arranjo a ser ordenado.
 * @param sz  O comprimento total do arranjo.
 *
 */
void bubble_sort (int *arr, unsigned int sz)
{
  // Este indicador será a condição de parada do algoritmo.
  int swapped;

  do {
    // A cada volta do laço, o indicador é sinalizado, logo no início, como
    // 'falso'.
    swapped = 0;

    // Aqui, efetuamos a enumeração do arranjo, partindo do segundo elemento em
    // diante.
    for (int i = 1; i < sz; ++i) {
      // Comparamos cada elemento do arranjo com seu antecessor.
      // Caso a condição se confirme, permutamos os dois valores e indicamos
      // esta permutação.
      if (arr[i - 1] > arr[i]) {
        swap(&arr[i - 1], &arr[i]);
        swapped = 1;
      }
    }

  // Quando não ocorrerem mais permutações, o arranjo inteiro está ordenado, e
  // o laço é concluído.
  } while (swapped);
}


int main ()
{
  show_algorithm(20U, bubble_sort, "Bubble sort");
  return 0;
}

