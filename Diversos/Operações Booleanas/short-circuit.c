/*
 * Operadores Lógicos e Curto-Circuito
 * ===================================
 *
 * Demonstra o recurso de Curto-Circuito sobre Operações Lógicas Booleanas.
 */

#include <stdio.h>
#include <stdlib.h>

#if __STDC_VERSION__ >= 199901L
# include <stdbool.h>
#else
# define bool int
# define false (0)
# define true  (1)
#endif

bool foo () { puts( "foo" ); return true;  }
bool bar () { puts( "bar" ); return false; }
bool baz () { puts( "baz" ); return true;  }

int main ()
{
  bool ret; /* NOTA: Necessária para a compilação do programa */

  puts( "Nas operações seguintes:" );
  printf( "  foo = %d\n",     foo() );
  printf( "  bar = %d\n",     bar() );
  printf( "  baz = %d\n\n\n", baz() );

  /*
   * Operador AND (&&)                                       +---+----+---+---+
   * -----------------                                       |   AND  |   y   |
   *                                                         |   &&   +---+---+
   * Para que a operação resulte numa afirmação              |        | F | V |
   * verdadeira, ambos os operandos precisam ser,            +---+----+---+---+
   * logicamente, verdadeiros. Para que a operação           |   |  F | F | F |
   * resulte numa afirmação falsa, basta que um dos          | x +----+---+---+
   * operandos ou ambos sejam falsos.                        |   |  V | F | V |
   *                                                         +---+----+---+---+
   * Seguindo essa lógica, como a operação é lida da
   * esquerda para a direita, basta que o operando à esquerda seja falso para
   * tornar a expressão falsa, não havendo necessidade de avaliar o segundo
   * operando. Caso contrário, o operando à direita precisa ser avaliado para
   * confirmar o resultado da expressão.
   *
   * Note que, nos exemplos a seguir, importa apenas como a operação funciona,
   * e não seus resultados. As operações lógicas são avaliadas conforme o valor
   * retornado pelas funções.
   */

  puts( "Operação foo() && baz()" );
  ret = foo() && baz(); /* Sempre avalia o segundo operando! */

  puts( "\nOperação bar() && baz()" );
  ret = bar() && baz(); /* Nunca avalia o segundo operando! */

  printf( "\n" );

  /*
   * Operador OR (||)                                        +---+----+---+---+
   * ----------------                                        |   OR   |   y   |
   *                                                         |   ||   +---+---+
   * De forma análoga ao operador AND, para que a            |        | F | V |
   * expressão seja considerada verdadeira, basta que        +---+----+---+---+
   * um dos operandos seja verdadeiro. Dessa maneira,        |   |  F | F | V |
   * quando o operando à esquerda for verdadeiro, a          | x +----+---+---+
   * expressão é verdadeira.                                 |   |  V | V | V |
   *                                                         +---+----+---+---+
   * O operando à direita só é considerado na
   * expressão quando o operando à esquerda é falso.
   */

  puts( "Operação foo() || baz()" );
  ret = foo() || baz(); /* Nunca avalia o segundo operando! */

  puts( "\nOperação bar() || baz()" );
  ret = bar() || baz(); /* Sempre avalia o segundo operando! */

  printf( "\n" );

  return EXIT_SUCCESS;
}

