/*
 *
 * Exercício de aula de Estruturas de Dados (21 de fevereiro de 2011).
 *
 */


#include <stdlib.h>
#include <stdio.h>


/* ************************************************************************* */


struct address {                        // Estrutura representando um endereço
  char* street;                         // físico contendo o nome da rua,
  char* city;                           // cidade, estado e código postal.
  char* state;
  char* zip;
};

// Cria a estrutura contendo os dados do endereço.
struct address create_address (
  char* street, char* city, char* state, char* zip )
{
  struct address address = { street, city, state, zip };
  return address;
}

// Formata o endereço para apresentação na tela.
void address_format ( char* buf, const struct address* a )
{
  const char* address_fmt =
    "   Endereço: %s\n"
    "   Cidade: %s\n"
    "   Estado: %s\n"
    "   CEP: %s\n";

  sprintf( buf, address_fmt, a->street, a->city, a->state, a->zip );
}


/* ************************************************************************* */


const char* month_names[] = {           // Nomes dos meses do ano
  "janeiro", "fevereiro", "março", "abril", "maio", "junho", "julho", "agosto",
  "setembro", "outubro", "novembro", "dezembro"
};


enum months {                           // Meses do ano
  JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER,
  NOVEMBER, DECEMBER
};


struct date {                           // Estrutura representando dia, mês e
  int day;                              // ano de uma determinada data.
  enum months month;
  int year;
};

// Cria a estrutura de data.
struct date create_date ( int day, enum months month, int year )
{
  struct date date = { day, month, year };
  return date;
}

// Formata a data para apresentação na tela.
void date_format ( char* buf, const struct date* d )
{
  const char* date_fmt = "%02d de %s de %d";
  sprintf( buf, date_fmt, d->day, month_names[d->month], d->year );
}


/* ************************************************************************* */


enum kind { LIFE, AUTO, HOME };         // O tipo da apólice.


struct policy_life {                    // Estrutura para a apólice de seguro
  struct date date_birth;               // de vida.
  char* beneficiary;
};


struct policy_auto {                    // Estrutura para a apólice de seguro
  int license_number;                   // automotivo.
  char* state;
  char* model;
  int year;
  float auto_deduct;
};


struct policy_home {                    // Estrutura para a apólice de seguro
  float home_deduct;                    // residencial.
  struct date date_built;
  int security;
};


struct policy {                         // Estrutura para os dados associados
  int policy_num;                       // a uma apólice de seguro.
  char* name;
  struct address address;
  int amount;
  float value;
  enum kind kind;

  union {                               // Uma 'union' mescla dados de vários
    struct policy_life policy_life;     // tipos num mesmo espaço de memória.
    struct policy_auto policy_auto;     // O acesso aos dados é feito através
    struct policy_home policy_home;     // do símbolo correspondente ao tipo
  } policy_kind;                        // que ele representa.
};

// Construtor para a estrutura da apólice de seguro de vida.
struct policy_life create_policy_life ( struct date date, char* beneficiary )
{
  struct policy_life policy_life = { date, beneficiary };
  return policy_life;
}

// Construtor para a estrutura da apólice de seguro automotivo.
struct policy_auto create_policy_auto (
  int license_number, char* state, char* model, int year, float auto_deduct )
{
  struct policy_auto policy_auto =
    { license_number, state, model, year, auto_deduct };

  return policy_auto;
}

// Construtor para a estrutura da apólice de seguro residencial.
struct policy_home create_policy_home (
  float home_deduct, struct date date_built, int security )
{
  struct policy_home policy_home =
    { home_deduct, date_built, security };

  return policy_home;
}

// Construtor para a estrutura da apólice.
//
// A partir do valor em 'kind', é determinada a forma de acesso à estrutura em
// 'policy_kind'.
struct policy create_policy (
  char* name, struct address address, int amount, float value,
  enum kind kind, void* policy_kind )
{
  static int policy_num = 0;
  struct policy policy;

  policy.policy_num = ++policy_num;
  policy.name = name;
  policy.address = address;
  policy.amount = amount;
  policy.value = value;
  policy.kind = kind;

  switch ( policy.kind ) {
  case LIFE:
    policy.policy_kind.policy_life = *((struct policy_life*) policy_kind);
    break;
  case AUTO:
    policy.policy_kind.policy_auto = *((struct policy_auto*) policy_kind);
    break;
  case HOME:
    policy.policy_kind.policy_home = *((struct policy_home*) policy_kind);
    break;
  }

  return policy;
}

// Formata e exibe uma apólice de seguro de vida.
void policy_life_printinfo ( const struct policy_life* p )
{
  char buf[64];
  const char* policy_life_printinfo_fmt =
    "***********************************************************************\n"
    " == Apólice de Seguro de Vida ==\n\n"
    " Data de Nascimento do Assegurado: %s\n"
    " Beneficiário: %s\n"
    "***********************************************************************\n"
    "\n";

  date_format( buf, &p->date_birth );

  printf( policy_life_printinfo_fmt, buf, p->beneficiary );
}


// Formata e exibe uma apólice de seguro automotivo.
void policy_auto_printinfo ( const struct policy_auto* p )
{
  const char* policy_auto_printinfo_fmt =
    "***********************************************************************\n"
    " == Apólice de Seguro Automotivo ==\n\n"
    " Modelo do Veículo: %s (%d)\n"
    " Licença: %d -- Estado: %s\n"
    " Dedução: %8.2f\n"
    "***********************************************************************\n"
    "\n";

  printf(
    policy_auto_printinfo_fmt, p->model, p->year, p->license_number,
    p->state, p->auto_deduct );
}


// Formata e exibe uma apólice de seguro residencial.
void policy_home_printinfo ( const struct policy_home* p )
{
  char buf[64];
  const char* policy_home_printinfo_fmt =
    "***********************************************************************\n"
    " == Apólice de Seguro Residencial ==\n\n"
    " Data da construção: %s\n"
    " Possui dispositivo de segurança?: %s\n"
    " Dedução: %8.2f\n"
    "***********************************************************************\n"
    "\n";

  date_format( buf, &p->date_built );

  printf(
    policy_home_printinfo_fmt, buf, ( p->security ? "SIM" : "NÃO" ),
    p->home_deduct );
}


// Formata e exibe uma apólice.
void policy_printinfo ( struct policy* p )
{
  char buf[512];
  const char* policy_printinfo_fmt =
    "***********************************************************************\n"
    " Apólice #%010d\n"
    " Assegurado: %s\n%s\n"
    " Valor do Seguro: R$ %8.2f -- Montante: %d\n";

  address_format( buf, &p->address );
  printf(
    policy_printinfo_fmt, p->policy_num, p->name, buf, p->value, p->amount );

  switch ( p->kind ) {                  // O restante da apresentação é feita
  case LIFE:                            // conforme o tipo da apólice.
    policy_life_printinfo( &p->policy_kind.policy_life );
    break;
  case AUTO:
    policy_auto_printinfo( &p->policy_kind.policy_auto );
    break;
  case HOME:
    policy_home_printinfo( &p->policy_kind.policy_home );
    break;
  }
}


/* ************************************************************************* */


int main ()
{
  struct policy_life p_life =
    create_policy_life( create_date( 6, JANUARY, 1985 ), "José Lopes" );

  struct policy P1 =
    create_policy( "Fulaninho da Silva",
      create_address( "Rua das Couves, 0", "Rio Branco", "Acre", "00000-000" ),
      5, 200000.0f, LIFE, &p_life
    );

  policy_printinfo( &P1 );

  /////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////

  struct policy_auto p_auto =
    create_policy_auto( 12345, "RJ", "Chevrolet Camaro SS", 2010, 0 );

  struct policy P2 =
    create_policy( "Fulaninho da Silva",
      create_address( "Rua das Couves, 0", "Rio Branco", "Acre", "00000-000" ),
      10, 300000.0f, AUTO, &p_auto
    );

  policy_printinfo( &P2 );

  /////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////

  struct policy_home p_home =
    create_policy_home( 0, create_date( 12, JUNE, 1991 ), 1 );

  struct policy P3 =
    create_policy( "José das Couves",
      create_address(
        "Alameda Besteraldo da Costa, 0", "Rio Branco", "Acre", "00000-000" ),
      30, 1500.0f, HOME, &p_home );

  policy_printinfo( &P3 );


  return EXIT_SUCCESS;
}

