/*
 *
 * Amostra de implementação de uma pilha de dados em C, e suas operações
 * básicas.
 *
 */

#include <stdio.h>
#include <stdlib.h>


#define MAX_STACK_SZ (20)


// Estrutura básica da pilha, com um arranjo limitado a MAX_STACK_SZ ítens.
struct stack {
  int array[MAX_STACK_SZ];
  int needle;
};


// Prepara uma pilha vazia com os parâmetros necessários.
struct stack stack_create ()
{
  struct stack S;
  S.needle = -1;

  return S;
}


// Responde se a pilha está vazia
int stack_empty ( struct stack* S )
{
  return ( S->needle < 0 );
}


// Retorna o elemento topo da pilha
int stack_top ( struct stack* S )
{
  if ( stack_empty(S) ) {
    // FIXME: Isto não deveria acontecer, mas...
    fprintf( stderr, "*** KABOOM! ***\n" );
    exit(1);
  }
  else {
    return S->array[ S->needle ];
  }
}


// Empilha um item.
void stack_push ( struct stack* S, int val )
{
  S->array[ ++S->needle ] = val;
  printf( "stack_push:\tS[%d] = %d\n", S->needle, S->array[ S->needle ] );
}


// Retira uma item da pilha, retornando-o como resultado.
int stack_pop ( struct stack* S )
{
  int val = stack_top( S );

  printf( "stack_pop:\tS[%d] = %d\n", S->needle, S->array[ S->needle ] );
  S->needle--;

  return val;
}


int main ()
{
  // Cria uma pilha vazia
  struct stack S = stack_create();

  stack_push( &S,  4 );                 // Preenchemos a pilha com uma
  stack_push( &S, 19 );                 // sequência de números inteiros.
  stack_push( &S,  3 );
  stack_push( &S, 25 );                 // Usamos '&S' indicando a posição da
  stack_push( &S, 11 );                 // pilha na memória.

  // Esvazia a pilha
  while ( !stack_empty( &S ) ) {
    stack_pop( &S );
  }

  return EXIT_SUCCESS;
}

