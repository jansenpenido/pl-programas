/*
 *
 * Amostra de implementação de uma fila de dados em C, e suas operações
 * básicas.
 *
 */

#include <stdio.h>
#include <stdlib.h>


#define FILA_TAM_MAX (20)


// Estrutura básica da pilha, com um arranjo limitado a FILA_TAM_MAX ítens.
struct fila {
  int vetor[FILA_TAM_MAX];
  int inicio;
  int fim;
};


// Prepara uma fila vazia com os parâmetros necessários.
struct fila fila_criar ()
{
  struct fila Q;
  Q.inicio = 0;
  Q.fim = 0;

  return Q;
}


// Responde se a fila está vazia
int fila_vazia ( struct fila* Q )
{
  return ( Q->inicio == Q->fim );
}


// Enfileira um item.
void fila_enfileirar ( struct fila* Q, int val )
{
  Q->vetor[ Q->fim ] = val;

  if ( Q->fim >= FILA_TAM_MAX - 1 ) {
    Q->fim = 0;
  }
  else {
    Q->fim++;
  }
}


// Retira o item mais antigo da fila, retornando-o como resultado.
int fila_remover ( struct fila* Q )
{
  int val = Q->vetor[ Q->inicio ];

  if ( fila_vazia(Q) ) {
    // FIXME: Isto não deveria acontecer, mas...
    fprintf( stderr, "*** KABOOM! ***\n" );
    exit(1);
  }
  else {
    if ( Q->inicio >= FILA_TAM_MAX - 1 ) {
      Q->inicio = 0;
    }
    else {
      Q->inicio++;
    }

    return val;
  }
}


int main ()
{
  // Cria uma fila vazia.
  struct fila F = fila_criar();

  fila_enfileirar( &F,  4 );      // Preenchemos a fila com uma
  fila_enfileirar( &F, 19 );      // sequência de números inteiros.
  fila_enfileirar( &F,  3 );      //
  fila_enfileirar( &F, 25 );      // Usamos '&F' indicando a posição da
  fila_enfileirar( &F, 11 );      // fila na memória.

  // Esvazia a fila.
  while ( !fila_vazia( &F ) ) {
    fila_remover( &F );
  }

  return EXIT_SUCCESS;
}

