/*
 *
 * Implementa��o de n� simples encadeado.
 *
 */

#include <stdlib.h>

#include "no.h"

/// Implementa��o de um n� encadeado.
struct no {
  int info;             ///< O dado contido no n� (um n�mero inteiro).
  struct no *anterior;  ///< A refer�ncia a um n� anterior.
};

/// Cria e faz o encadeamento de um um n�.
struct no *no_criar (int info, struct no *anterior)
{
  struct no *N = malloc(sizeof(struct no));
  N->info = info;
  no_orientar(N, anterior);

  return N;
}

/// Destr�i um n� encadeado.
void no_destruir (struct no *N)
{
  free(N);
  N = 0;
}

/// Retorna o valor contido em um n�.
int no_info (const struct no *N)
{
  return (N ? N->info : 0);
}

/// Retorna a refer�ncia ao n� anterior, ou nulo, de uma determinado n�.
struct no *no_anterior (const struct no *N)
{
  return (N ? N->anterior : 0);
}

/// Altera a orienta��o de um n�.
void no_orientar (struct no *N, struct no *n)
{
  if (N) { N->anterior = n; }
}

