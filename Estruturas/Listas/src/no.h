/*
 *
 * Implementa��o de n� simples encadeado.
 *
 */

#ifndef __ESTRUTURAS_NO_H__
#define __ESTRUTURAS_NO_H__

struct no *no_criar (int, struct no *);
void no_destruir (struct no *);
int no_info (const struct no *);
struct no *no_anterior (const struct no *);
void no_orientar (struct no *, struct no *);

#endif //__ESTRUTURAS_NO_H__

