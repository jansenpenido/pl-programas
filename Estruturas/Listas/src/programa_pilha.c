/*
 *
 * Programa de demonstra��o da implementa��o de pilha em C.
 *
 */

#include <stdio.h>

#include "util.h"
#include "pilha.h"


int main ()
{
  struct pilha *P = pilha_criar();

  pilha_inserir(P,  4); // Preenchemos a pilha com uma
  pilha_inserir(P, 19); // sequ�ncia de n�meros inteiros.
  pilha_inserir(P,  3);
  pilha_inserir(P, 25);
  pilha_inserir(P, 11);

  // Quanto elementos h� na pilha?
  printf("pilha_tamanho(P) = %u\n\n", pilha_tamanho(P));

  // Esvazia a pilha
  while (!pilha_vazia(P)) {
    printf("pilha_remover(P) = %d\n", pilha_remover(P));
  }

  PAUSE;
  return 0;
}

