/*
 *
 * Amostra de implementa��o de uma fila de dados em C, e suas opera��es
 * b�sicas, usando aloca��o din�mica de mem�ria e ponteiros.
 *
 */

#ifndef __ESTRUTURAS_FILA_H__
#define __ESTRUTURAS_FILA_H__

struct fila;

struct fila *fila_criar ();
void fila_destruir (struct fila *);
struct no *fila_inicio (const struct fila *);
struct no *fila_fim (const struct fila *);
int fila_vazia (const struct fila *);
void fila_inserir (struct fila *, int);
int fila_remover (struct fila *);
unsigned int fila_tamanho (const struct fila *);

#endif //__ESTRUTURAS_FILA_H__

