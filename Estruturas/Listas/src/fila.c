/*
 *
 * Amostra de implementa��o de uma fila de dados em C, e suas opera��es
 * b�sicas, usando aloca��o din�mica de mem�ria e ponteiros.
 *
 */

#include <stdlib.h>

#include "no.h"
#include "fila.h"


/// Estrutura de controle da fila.
struct fila {
  struct no *inicio;          ///< In�cio da fila
  struct no *fim;             ///< Final da fila
  unsigned int contagem;      ///< Contagem de elementos
};

/// Prepara uma fila vazia.
struct fila *fila_criar ()
{
  struct fila *F = calloc(1, sizeof(struct fila));
  return F;
}

/// Destr�i um fila.
void fila_destruir (struct fila *F)
{
  free(F);
  F = 0;
}

/// Retorna o elemento do in�cio da fila.
struct no *fila_inicio (const struct fila *F)
{
  return (F ? F->inicio : 0);
}

/// Retorna o elemento do fim da fila.
struct no *fila_fim (const struct fila *F)
{
  return (F ? F->fim : 0);
}

/// Responde se a fila est� vazia.
int fila_vazia (const struct fila *F)
{
  return (fila_inicio(F) == 0);
}

/// Insere um novo elemento no final da fila.
void fila_inserir (struct fila *F, int valor)
{
  if (F) {
    struct no *N = no_criar(valor, 0);

    if (!fila_inicio(F)) { F->inicio = N; }
    if (fila_fim(F)) { no_orientar(F->fim, N); }

    F->fim = N;
    ++F->contagem;
  }
}

/// Remove um item do in�cio da fila, retornando seu dado como resultado.
int fila_remover (struct fila *F)
{
  int valor = 0;

  if (F && !fila_vazia(F)) {
    struct no *N = fila_inicio(F);

    valor = no_info(N);
    F->inicio = no_anterior(N);
    --F->contagem;

    no_destruir(N);
  }

  return valor;
}

/// Retorna a contagem de elementos na fila.
unsigned int fila_tamanho (const struct fila *F) {
  return (F ? F->contagem : 0);
}

