/*
 *
 * Amostra de implementa��o de uma pilha de dados em C, e suas opera��es
 * b�sicas, usando aloca��o din�mica de mem�ria e ponteiros.
 *
 */

#ifndef __ESTRUTURAS_PILHA_H__
#define __ESTRUTURAS_PILHA_H__

struct pilha;

struct pilha *pilha_criar ();
void pilha_destruir (struct pilha *);
struct no *pilha_topo (const struct pilha *);
int pilha_vazia (const struct pilha *);
void pilha_inserir (struct pilha *, int);
int pilha_remover (struct pilha *);
unsigned int pilha_tamanho (const struct pilha *);

#endif //__ESTRUTURAS_PILHA_H__

