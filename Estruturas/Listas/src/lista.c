/*
 *
 * Amostra de implementa��o de uma lista _duplamente ligada_ e suas principais
 * opera��es.
 *
 * A implementa��o faz uso de aloca��o din�mica de mem�ria.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/// Estrutura que representa o n� em uma lista
struct no {
  int val;              ///< Um valor inteiro. Poderia ser qualquer coisa...
  struct no *anterior;  ///< Ponteiro para o n� anterior.
  struct no *proximo;   ///< Ponteiro para o pr�ximo n�.
};

/// Cria um n� com um dado valor.
struct no *no_criar (int val)
{
  struct no* N = calloc(1, sizeof(struct no));
  N->val = val;

  return N;
}

/// Destr�i o n�.
void no_destruir (struct no* N)
{
  free(N);
  N = 0;
}

/// Retorna o valor contido no n�.
int no_valor (struct no *N)
{
  return (N ? N->val : 0);
}

/// Retorna o pr�ximo n�.
struct no *no_anterior (struct no* N)
{
  return (N ? N->anterior : 0);
}

/// Retorna o n� anterior.
struct no *no_proximo (struct no* N)
{
  return (N ? N->proximo : 0);
}


/// Estrutura de controle da lista
struct lista {
  struct no *inicio;    ///< A cabe�a da lista.
  struct no *fim;       ///< A ponta da lista.
};

/// Cria uma lista.
struct lista *lista_criar ()
{
  struct lista *L = calloc(1, sizeof(struct lista));
  return L;
}

/// Destr�i uma lista.
void lista_destruir (struct lista *L)
{
  free(L);
  L = 0;
}

/// Retorna o primeiro n� (a cabe�a da lista).
struct no *lista_inicio (struct lista *L)
{
  return (L ? L->inicio : 0);
}

/// Retorna o �ltimo n� (a ponta da lista).
struct no *lista_fim (struct lista *L)
{
  return (L ? L->fim : 0);
}

/// Busca um item da lista.
struct no *lista_buscar (struct lista *L, int k)
{
  struct no *N = 0;

  if (L) { N = lista_inicio(L); }

  while ((N != 0) && (N->val != k)) {
    N = no_proximo(N);
  }

  return N;
}

/// Insere um novo item � lista.
void lista_inserir (struct lista *L, int k)
{
  if (L) {
    struct no *N = no_criar(k);

    N->anterior = lista_fim(L);

    if (lista_fim(L) != 0) {
      L->fim->proximo = N;
    }

    if (lista_inicio(L) == 0) {
      L->inicio = L->fim;
    }

    L->fim = N;
    N->proximo = 0;
  }
}

/// Remove um item da lista.
void lista_remover (struct lista *L, int k)
{
  if (L) {
    struct no *N = lista_buscar(L, k);
    if (!N) { return; }

    if (no_anterior(N) != 0) {
      N->anterior->proximo = no_proximo(N);
    }
    else {
      L->inicio = no_proximo(N);
    }

    if (N == lista_fim(L)) {
      L->fim = no_anterior(N);
    }

    if (no_proximo(N) != 0) {
      N->proximo->anterior = no_anterior(N);
    }
  }
}


int main ()
{
  struct lista *L = lista_criar();      // Criando uma lista vazia.

  lista_inserir(L, 45);                 // Adicionando itens a lista.
  lista_inserir(L, 32);                 // Cada item adicionado passa a ser a
  lista_inserir(L,  9);                 // cabe�a da lista.
  lista_inserir(L, 38);
  lista_inserir(L, 26);
  lista_inserir(L, 18);

  // Enumerando os itens � partir do mais recente.
  for (struct no *N = lista_inicio(L); N != 0; N = no_proximo(N)) {
    printf("N�: %d\n", no_valor(N));
  }
  puts("\n");

  lista_remover(L, 45);                 // Removendo itens.
  lista_remover(L, 15);                 // Somente itens presentes na lista
  lista_remover(L, 32);                 // s�o removidos.

  for (struct no *N = lista_inicio(L); N != 0; N = no_proximo(N)) {
    printf("N�: %d\n", no_valor(N));
  }

  return EXIT_SUCCESS;
}

