/*
 *
 * Programa de demonstra��o da implementa��o de fila em C.
 *
 */

#include <stdio.h>

#include "util.h"
#include "fila.h"


int main ()
{
  struct fila *F = fila_criar();

  fila_inserir(F,  4); // Preenchemos a fila com uma
  fila_inserir(F, 19); // sequ�ncia de n�meros inteiros.
  fila_inserir(F,  3);
  fila_inserir(F, 25);
  fila_inserir(F, 11);

  // Quanto elementos h� na fila?
  printf("fila_tamanho(F) = %u\n\n", fila_tamanho(F));

  // Esvazia a fila
  while (!fila_vazia(F)) {
    printf("fila_remover(F) = %d\n", fila_remover(F));
  }

  PAUSE;
  return 0;
}

