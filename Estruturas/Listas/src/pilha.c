/*
 *
 * Amostra de implementa��o de uma pilha de dados em C, e suas opera��es
 * b�sicas, usando aloca��o din�mica de mem�ria e ponteiros.
 *
 */

#include <stdlib.h>

#include "no.h"
#include "pilha.h"


/// Estrutura de controle da pilha.
struct pilha {
  struct no* top;             ///< O elemento topo da pilha
  unsigned int contagem;      ///< Contagem de elementos
};

/// Prepara uma pilha vazia.
struct pilha *pilha_criar ()
{
  struct pilha *P = calloc(1, sizeof(struct pilha));
  return P;
}

/// Destr�i um objeto de pilha.
void pilha_destruir (struct pilha *P)
{
  free(P);
  P = 0;
}

/// Retorna o elemento topo da pilha.
struct no *pilha_topo (const struct pilha *P)
{
  return (P ? P->top : 0);
}

/// Responde se a pilha est� vazia.
int pilha_vazia (const struct pilha *P)
{
  return (pilha_topo(P) == 0); // Se n�o h� topo, n�o h� elementos.
}

/// Coloca um novo elemento no topo da pilha.
void pilha_inserir (struct pilha *P, int valor)
{
  if (P) {
    struct no *N = no_criar(valor, pilha_topo(P));
    ++P->contagem;
    P->top = N;
  }
}

/// Retira uma item da pilha, retornando seu dado como resultado.
int pilha_remover (struct pilha *P)
{
  int valor = 0;

  if (P && !pilha_vazia(P)) {
    struct no *N = pilha_topo(P);

    valor = no_info(N);
    P->top = no_anterior(N);  // Define como novo topo o elemento abaixo.
    --P->contagem;
    no_destruir(N);
  }

  return valor;
}

/// Retorna a contagem de elementos na pilha.
unsigned int pilha_tamanho (const struct pilha *P) {
  return (P ? P->contagem : 0);
}

