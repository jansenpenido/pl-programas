/*
 * O programa faz a leitura de dois vetores de inteiros, cada um com 5
 * posições de largura. O primeiro recebe apenas números divisíveis por 2 ou
 * por 3, e o segundo, apenas números não divisíveis por 5. Ao final, é
 * apresentada uma tabela com os valores lidos para os vetores.
 */


#include "validador.h"


int main ()
{
  int vetor_a[L_VETOR_MAX];
  int vetor_b[L_VETOR_MAX];

  le_vetor( vetor_a, L_VETOR_MAX, "Digite o %2dº elemento do vetor A: ", validar_divisivel_por_2_ou_3 );
  le_vetor( vetor_b, L_VETOR_MAX, "Digite o %2dº elemento do vetor B: ", validar_nao_divisivel_por_5 );

  printf(
    "\n"
    "| Vetor A | Vetor B |\n"
    "+---------+---------+\n"
  );
  for ( int i = 0; i < L_VETOR_MAX; i++ ) {
    printf( "| %7d | %7d |\n", vetor_a[i], vetor_b[i] );
  }

  return 0;
}

