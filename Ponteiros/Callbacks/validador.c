#include "validador.h"


bool validar_divisivel_por_2_ou_3 ( int valor )
{
  return ((valor % 2 == 0) || (valor % 3 == 0));
}


bool validar_nao_divisivel_por_5 ( int valor )
{
  return (valor % 5 != 0);
}


int le_valor ( bool (* validador)(int) )
{
  bool e_valido;
  int valor;

  do {
    scanf( "%d", &valor );
    e_valido = validador( valor );

    if ( !e_valido ) {
      printf( "Valor inválido. Digite outro valor para o elemento: " );
    }
  } while ( !e_valido );

  return valor;
}


void le_vetor ( int* vetor, int largura, char* mensagem, bool (* validador)(int))
{
  for ( int i = 0; i < largura; i++ ) {
    printf( mensagem, i + 1 );
    vetor[i] = le_valor( validador );
  }
  printf( "\n" );
}

