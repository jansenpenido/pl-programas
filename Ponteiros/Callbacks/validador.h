/*
 *
 */

#ifndef _RB_VALIDADOR_H_
#define _RB_VALIDADOR_H_

#include <stdio.h>
#include <stdbool.h>


#define L_VETOR_MAX ( 5)


/**
 * Valida os números divisíveis por 2 ou por 3.
 */
bool validar_divisivel_por_2_ou_3 ( int valor );

/**
 * Valida os números não divisíveis por 5.
 */
bool validar_nao_divisivel_por_5 ( int valor );

/**
 * Faz a leitura dos valores, retornando apenas os valores válidos.
 *
 * A validação é feita pela callback, que é passada como parâmetro da função.
 */
int le_valor ( bool (* validador)(int) );

/**
 * Faz a leitura dos valores no vetor.
 *
 * O valores lidos no vetor, cuja largura é informada no segundo parâmetro, são
 * validados pela callback, informada no quarto parâmetro.
 *
 * A função ainda exibe uma mensagem personalizada na a leitura de cada vetor.
 */
void le_vetor ( int* vetor, int largura, char* mensagem, bool (* validador)(int));


#endif // _RB_VALIDADOR_H_

